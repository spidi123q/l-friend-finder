<?php
function displayDate($date_time){
  $year = substr($date_time,0,4);
  $month = substr($date_time,5,2);
  $day = substr($date_time,8,2);
  $string = "";
  if((date("Y")-$year) == 0){

    if ( (date("m") - $month) == 0) {
      # code...
      if ( (date("d") - $day) <= 1) {
        # code...
        $string = ((date("d") - $day)== 0)?"today":"yesteday";
      }
      else {
        $string = (date("d") - $day)." days ago";
      }
    }
    else {
       $string = (((date("m") - $month)) == 1)?"1 month ago":(date("m") - $month)." months ago";
    }
  }
  else {
    $string = (((date("Y") - $year)) == 1)?"1 year ago":(date("Y") - $year)." years ago";
  }
  return $string;
}


 ?>
