<?php
function resize_image($file, $w, $h, $crop=FALSE) {
    list($width, $height) = getimagesize($file);
    $r = $width / $height;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width-($width*abs($r-$w/$h)));
        } else {
            $height = ceil($height-($height*abs($r-$w/$h)));
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
    }
    $explode = explode(".",$file);
    $ext = end($explode);
    if($ext == 'jpg' || $ext == 'JPG' || $ext == 'jpeg' || $ext == 'JPEG'){

      $src = imagecreatefromjpeg($file);
      $dst = imagecreatetruecolor($newwidth, $newheight);
      imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
      $imageName = basename($file);
      imagejpeg($dst,'images/userimages/temp/'.$imageName);
      return $dst;

    }
    else if($ext == 'png' || $ext == 'PNG'){
      $src = imagecreatefrompng($file);
      $dst = imagecreatetruecolor($newwidth, $newheight);
      imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
      $imageName = basename($file);
      imagepng($dst,'images/userimages/temp/'.$imageName);
      return $dst;
    }
    else if($ext == 'gif' || $ext == 'GIF'){
      $src = imagecreatefromgif($file);
      $dst = imagecreatetruecolor($newwidth, $newheight);
      imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
      $imageName = basename($file);
      imagegif($dst,'images/userimages/temp/'.$imageName);
      return $dst;
    }

}

 ?>
