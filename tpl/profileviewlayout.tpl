<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>[@title]</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<body>
	<div id="content">
		<div id="username">[@content]</div>
		<div id="dp">
			<img src="[@photoURL]" class="photo" alt="[@name]" />
		</div>

	<div id="info">
		<tr><b>Age :</b> [@age]<br></tr>
		<tr><b>Country :</b> [@country]<br></tr>
		<tr><b>Last online :</b> [@last]<br></tr>
	</div>
</div>
</body>
</html>
